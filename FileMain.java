/*
 * Plik pokazuej możliości zapisu i odczytu pliku z dysku twardego. W naszym wypadku zapisywany/odczytywany będzie plik
 * tekstowy, jednak Java pozwala także na zachowywanie danych binarnych. Dodatkowo program wykorzysta argumenty (parametry)
 * wejściowe, gdzie jednym z parametrów będzie nazwa pliku do zapisu i odczytu, drugim zaś ilość liczb, jaką będziemy chcieli 
 * wylosować. Możliwe jest podawanie (teoretycznie) dowolnej liczby parametrów wejściowych oraz dowolnej ich interpretacji.
 * Każdy parametr wejściowy jest domyślnie traktowany jako ciąg znakowy (string).
 * 
 * PRZYKŁAD ODPALENIA PROGRAMU:
 * 
 * java.exe FileMain.java -wejscie nowyplik.txt -liczby 15
 * 
 * oznacza to, że plik zapisu/odczytu będzue nazywal się nowyplik.txt, a ilość zapisanych w nim liczb będzie wynosiłą 15
 * 
 * java.exe FileMain.java -liczby 20 -wejscie plik.txt
 * 
 * plik wejścia/wyjścia będzie nazywał się plik.txt, a ilość zapisanych w nim liczb wyniesie 20
 * 
 * java.exe FileMain.java
 * 
 * plik wejścia/wyjścia nie otrzyma żadnej nazwny, nie zostanie też wylosowana żadna liczba (to wynika z dalszej części programu)
 * 
 * Tak więc parametry wejściowe podaje się zaraz po nazwie odpalanego programu; pozwalają one na wstępne ustawienie niektorych wartości zmiennych
 * dostępnych w programie bez nadawania ich interaktywnym menu.
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class FileMain {
	public static void main(String[] args) {
		//na początek tworzymy dwie zmienne
		//pierwsza z nich przechowuej ilość liczb do wylosowania; 
		//nadajemy jej wartość 0 (inicjalizujemy ją), więc w najgorszym wypadku
		//program nie wylosuje nam żadnych wartości
		int countNums=0;
		//druga zmienna będzie przechowywać nazwę pliku, do którego będziemy zapisywać wylosowane liczby
		//na potrzeby tego materiału plik ten też, zaraz po zapisie, będzie odczytywany celem
		//wyświetelnia liczb w nim zachowanych (program nie będzie ich przechowywał w pamięci RAM!)
		String inFile = null;
		//robimy pętlę for, która odczytuje kolejne parametry zapisane w tablicy parametrów (pod każdym indeksem tablicy
		//zapisany jest dokładnie jeden wyraz/wartość)
		//UWAGA - program zakłada, że każdy parametr ma swoją nazwę; nazwę parametru poprzedza pojedynczy myślnik; podobne
		//rozwiązanie można spotkać chociażby w Windows PowerShell, jednak możliwe jest też używanie parametrów względem ich
		//umiejscowienia (pierwszy, drugi trzeci... decyduje kolejność ich podania) lub poprzez inne znaki poprzedzające (np. w 
		//bash systemu Linux pojedynczy myślnik zarezerwowany jest dla 'inicjału' nazwy parametru, podwójny myślnik zaś pozwala
		//na podanie pełnej nazwy parametru.
		for(int i=0;i<args.length;i++) {
			//przyjęliśmy, że nazwa parametru poprzedzona jest myślnikiem; jeżeli go znalezliśmy - przechodzimy do kolenych kroków
			if (args[i].charAt(0) == '-') {
				//w warunku wyliczeniowym jako parametr podajemy nazwę parametru, z pominięciem myślnika; w tym celu
				//wykorzystujemy metodę String o nazwie substring, która z bazowego ciągu znakowego wycina podciąg
				//zaczynający się od wskazanego znaku (w tym wpadku pomijamy znak na wartości 0 - czyli myślnik)
				switch (args[i].substring(1)) {
				//jeżeli parametr nazywa się wejscie
				case "wejscie":
					//sprawdzamy czy w tablicy jest kolejna wartość; jeżeli tak 
					if((++i)<args.length) {
						//to przypisujemy jej wartość do odpowiedniej zmiennej
						inFile = args[i];
					}
					break;
					//analigicznie działa poniższy przypadek
				case "liczby":
					if(++i<args.length)
						countNums=Integer.valueOf(args[i]);
					break;
				default:
					break;
				}
			}
			//ta pęta for, jak można zauważyć, działa nieco inaczej niż dotychczas poznane; w przypadku odnalezienia odpowiedniej nazyw
			//wartość i zwiększana jest wewnątrz warunków; pokazuje to, że zmiana wartości i może następować także w ciele samego for
		}
		//tworzymy nową zmienna klasy FileOutputStream, która będzie odpowiedzialna za zapis do pliku
		FileOutputStream nowyPlik;
		//każde działanie na plikach - otwarcie, zapis, odczyt czy zamknięcie - musi zostać otoczone klauzulą try {} catch () {}
		//trzbea mieć na uwadze, że każda operacja, nad którą program w języku java nie może bezpośrednio zapanować (w tym wypadku
		//wiele zależy od systemu plików, dysku, uprawnień) powinna znajdować się w tej klauzuli; to pozwala wyeliminować 
		//błędy działania aplikacji (jeżeli np. plik nie może zostać otwarty to program, zamiast przestać działać, 
		//wyświetli odpowiedni komunikat i będzie działał dalej)
		try {
			//otwieramy plik do zapisu; jeżeli plik nie istnieje - zostanie utworzony
			nowyPlik = new FileOutputStream(inFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//jeżeli plik się nie otworzy - nie warto by program dalej działał więcj wychodzimy z funkcji main
			return;
		}
		//skoro mamy otwarty plik - zaczynamy losować liczby i dorzucać je do pliku
		for(int i=0;i<countNums;i++) {
			//wszystko wykonuje się jak poprzednio w klauzuli try{} catch() {} - ten sam powód co powyżej
			try {
				//losujemy liczbę z zakresu do 40; bez podania ograniczenia liczby losują się z maksymalnego akresu (-2147483648 -> 2147483647)
				int tmp = new Random().nextInt(40);
				//wylosowana liczba zostaje zapisana do pliku poprzez metodę write; metoda ta może zapisywać bajty (tablicę bajtów)
				//wartości liczb całkowitych lub tablicę bajtów, gdzie określamy od którego bajtu (w tablicy) zaczniemy zapis oraz na którym skończymy
				
				//dla naszego wykorzystania wybierzemy pierwszą opcję; ponieważ ciąg znakowy jest po części tablicą bajtów (ale tylko tych drukowalnych)
				//toteż musimy zamienić go na bajty. Ponadto przed tą operacją musimy zamienić wylosowaną wartość (int) na String. Stąd:
				//- zamieniamy wartość tmp na String -> (String.valueOf(tmp)
				//- aby każda nowo wylosowana liczba była zapisywana w nowej linii pliku dodajemy znak nowej linii -> String.valueOf(tmp) + "\n"
				//- teraz cały ten ciąg musimy zamienić na ciąg bajtów; najpierw informujemy kompilator, że ma zamienić 
				//powyższe wyrażenie na jeden nowy ciąg znakowy (wykorzystujemy niejawny konstuktor klasy String) -> (String.valueOf(tmp) + "\n")
				//- Java traktuje powyższe złożenie jako nowy obiekt; nic nie stoi na przeszkodzie, by wykorzystać dowolną metodę bądz właściwość
				//obiektu String, jak np. zwrócenie go w postaci tablicy bajtowej -> (String.valueOf(tmp) + "\n").getBytes()
				nowyPlik.write((String.valueOf(tmp) + "\n").getBytes());
				//poniżej przykład bezpośredniego zapisu wartości do pliku; plik zostanie zapisany w postaci binarnej (mniejszy rozmiar, dla nas
				//nieczytelny)
				//nowyPlik.write(new Random().nextInt());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//zamknięcie pliku; konieczność wykorzystania try {} catch() {}
		try {
			nowyPlik.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//utworzenie zmiennej typu FileInputStream, która pozwoli nam na obsługę czytania pliku (wejście z pliku)
		FileInputStream wczytajPlik;
		try {
			wczytajPlik= new FileInputStream(inFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		//zmienna pomocnicza, do której będą trafiać kolejne bajty (cyfry) liczby
		String odczyt="";
		//zmienna pomocnicza; każdy wczytany bajt będzie do niej trafiał; inicjalizacja wstawia pod znak 
		//wartość null
		char a='\0';
		try {
			//pętla będzie wykonywała się do chwili, gdy w pliku znajdują się jakiekolwiek dane do wczytania
			//metoda zwraca pierwotnie całą długość pliku (ilość bajtów do wczytania) i wartość ta jest pomniejszana
			//każdorazowo gdy jakikolwiek bajt zostanie odczytany
			while (wczytajPlik.available()>0) {
				//za każdym przejściem pętli czyścimy wartość odczyt
				odczyt="";
				try {
					//w tej pętli wykonujemy następujące operacje:
					//- do zmiennej a przypisujemy jeden oeczytany bajt metodą read(); ponieważ bajt posiada wartość liczbową
					//musimy zamienić jego wartość na char (wykonujemy to jako konwersję jawną w linii) -> (char)wczytajPlik.read()
					//- odczytaną w ten sposób wartość, tak jak zostało to już wspomniane, przypisujemy to zmiennej a -> a=(char)wczytajPlik.read()
					//- powyższe polecenie musimy traktować jako pojedyncze polecenie warunku (bo while przyjmuje jako parametr wartości true lub false
					//na pewno nie pozwala od tak sobie przypisywać wartości zmiennych w warunku!); dlatego obejmujemy je nawiasami co, podobnie jak to miało
					//miejsce w funkcji write, niejawnie tworzy nam obiekt Character z wartością przepisaną z read()
					//teraz wartość tą sprawdzamy czy nie jest ona równa wartości znaku '\n' -> końca linii
					//do chwili, kiedy warunek będzie spełniony (czy odczytany znak nie będzie końcem linii), dodajemy odczytaną wartość (znak)
					//do zmiennej odczyt (czyli dokładamy kolejne cyfry do zapisanej w linii liczby)
					while((a=(char)wczytajPlik.read())!='\n')
						odczyt+=a;
					//jeżeli powyższa pętla się zakończy, wyświetalmy odczytaną liczbę na wyjściu. Możemy z nią w zasadzie robić 
					//co zechcemy - dodawać, odejmować, przepisywać do innego pliku
					System.out.println(odczyt);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * alternatywne wczytanie pliku; wczytanie pozwala na przechwycenie wszystkich bajtów w pliku do zmiennej typu byte[]
		 * Trzeba jednak brać pod uwagę, że taki odczyt będzie dla nas nieczytelny i będziemy musieli zastosować podobny mechanizm
		 * do tego powyżej by odpowiednio odczytać wartości tekstowe zmiennych
		 */
//		byte[] b = null;
//		String str = "";
//		try {
//			b = new byte[wczytajPlik.available()];
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		try {
//			b= wczytajPlik.readAllBytes();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.print(b.length);
	}
	
	 
}
